module Main where

{-# LANGUAGE OverloadedLists #-}

import System.Random ( Random(randomRs), RandomGen )

main :: IO ()
main = putStrLn (display_list show [[1,2,3],[4,5,6]])
tripleDisplay :: Show a => [[a]] -> IO()
tripleDisplay inp = putStrLn (display_list (display_list show) inp)

type Architecture = [Int]
type ArchitecturePair = [(Int,Int)]
type Matrices = [Matrix]
-- Considered 'top-down'
type Matrix = [Vector]
type Vector = [Float]

pairsize :: (Int,Int) -> Int
pairsize (x,y) = x*y
architecture_pairs :: Architecture -> ArchitecturePair
architecture_pairs (h:w:[]) = 
    (h+1,w):[]
architecture_pairs (h:w:r) = 
    (h+1,w):(architecture_pairs (w:r))

architectureOf :: [[[a]]] -> Architecture
architectureOf [] = []
architectureOf ((xff:_):xs) =
    ((length xff)-1):(architectureOf xs)
architecture_size :: Architecture -> Int
architecture_size (x:xs:xt) =
    x*xs
    + architecture_size (xs:xt)
architecture_size _ = 0

toLists :: [Float] -> [(Int,Int)] -> [[[Float]]]
toLists r [] = []
toLists r (p:ps) = 
    (reshapeList p (take k r)):(toLists (drop k r) ps)
    where k = pairsize p
initaliseThetas :: RandomGen r => r -> Float -> Architecture -> Matrices
initaliseThetas r epsilon arch = 
    toLists 
        (randomRs (-epsilon,epsilon) r) 
        (architecture_pairs arch)
display_list :: (a->String) -> [a] -> String
display_list _ [] =
    ""
display_list f (af:as) =
    (f af)++'\n':(display_list f as)
reshapeList :: (Int,Int) -> [a] -> [[a]]
reshapeList (h,0) xs = 
    []
reshapeList (h,w) xs = 
    (take h xs):(reshapeList (h,w-1) (drop h xs))

-- FORPROP
-- generates a list of activations given a list of Thetas and initial values
activate :: [[[Float]]] -> [Float] -> [[Float]]
activate [] am = 
    am:[]
activate (t:ts) am = 
    am:(activate ts ac)
    where ac = map sigmoid ((1:am)-*t)

-- COST CALCULATION
cost :: Vector -> Vector -> Matrices -> Float -> Float
cost y ya ths lambda =
    innacuaracy y ya n
    + regularcost ths lambda
    where n = length y
innacuaracy :: Vector -> Vector -> Int -> Float
innacuaracy y ya n = sum (zipWith costs y ya)/n'
    where n' = fromIntegral n
regularcost :: Matrices -> Float -> Float
regularcost ths lambda =
    (lambda/m)*sum[sum[sum(ts)|(_:ts)<-th]|th<-ths]
    where
        m = fromIntegral (architecture_size (architectureOf ths))
costs :: Float -> Float -> Float
costs 1.0 ya = -log(ya)
costs _ ya = -log(1-ya)

-- some fun functions
sigmoid :: Float -> Float
sigmoid z = 1/(1+exp(-z))
balance :: Float -> Float
balance a = a*(1-a)
mmap :: (a->b)->[[a]]->[[b]]
mmap f m = [map f ms|ms<-m]

-- the matrix has vectors going down
(-*) :: Num a => [a] -> [[a]] -> [a]
(-*) v m = [v.*mm | mm <- m]
-- dot product
(.*) :: Num a => [a] -> [a] -> a
(.*) [] _ = 0
(.*) _ [] = 0
(.*) (v:vs) (u:us) = v*u + vs.*us

ssum x = sum(map sum x)
